package com.tadmar.portal.PortalMain.controller;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named(value="LinkFCtrl")
@ViewScoped
public class LinkFCtrl implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String linkF;

	/********************************************************************************************************
	 *  Constructors
	 * @throws UnsupportedEncodingException 
	 ********************************************************************************************************/
	@PostConstruct
	private void init() throws UnsupportedEncodingException {
		
		String urlPath = FacesContext.getCurrentInstance().getExternalContext().getRequestServletPath();
    	Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String urlLinkF = (String) params.get("linkf");
		linkF = URLDecoder.decode(urlLinkF, StandardCharsets.UTF_8.name());
		
	}

	public String getLinkF() {
		return linkF;
	}
	public void setLinkF(String linkF) {
		this.linkF = linkF;
	}
}

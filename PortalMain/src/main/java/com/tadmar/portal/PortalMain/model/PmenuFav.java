package com.tadmar.portal.PortalMain.model;

import java.io.Serializable;
import java.util.List;

public class PmenuFav implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PmenuF pmheader;
	private List<PmenuF> pmline;

	public PmenuF getPmheader() {
		return pmheader;
	}
	public void setPmheader(PmenuF pmheader) {
		this.pmheader = pmheader;
	}
	public List<PmenuF> getPmline() {
		return pmline;
	}
	public void setPmline(List<PmenuF> pmline) {
		this.pmline = pmline;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pmheader == null) ? 0 : pmheader.hashCode());
		result = prime * result + ((pmline == null) ? 0 : pmline.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PmenuFav))
			return false;
		PmenuFav other = (PmenuFav) obj;
		if (pmheader == null) {
			if (other.pmheader != null)
				return false;
		} else if (!pmheader.equals(other.pmheader))
			return false;
		if (pmline == null) {
			if (other.pmline != null)
				return false;
		} else if (!pmline.equals(other.pmline))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PmenuFav [pmheader=" + pmheader + ", pmline=" + pmline + "]";
	}
}

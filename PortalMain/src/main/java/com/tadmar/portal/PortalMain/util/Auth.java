package com.tadmar.portal.PortalMain.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class Auth {
	
	private void Auth() {}
	
	public static final String getUserName() {
		HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	    String userName = servletRequest.getUserPrincipal().getName().toUpperCase().trim().replace("TDM\\", "").replace("TDM/","");
	    return userName;
	}
}

package com.tadmar.portal.PortalMain.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Enumeration;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class Utils {
	
    public static final void dspMsg(Severity severity, String msgText) {
    	FacesContext ctxt = FacesContext.getCurrentInstance();
    	ctxt.addMessage(null, new FacesMessage(severity , msgText ,  "") );

    }

	public static final String getUserName() {
		HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	    //Enumeration<String> headerNames = servletRequest.getHeaderNames();
	    //while (headerNames.hasMoreElements()) {
	    //    String headerName = headerNames.nextElement();
	    //    String headerValue = servletRequest.getHeader(headerName);
	    //    System.out.println("Header Name:" + headerName + " " + headerValue);
	    //}     
	    String userName = servletRequest.getUserPrincipal().getName().toUpperCase().trim().replace("ZD\\", "").replace("ZD/", "")
	    		.replace("TDM\\", "").replace("TDM/","");
	    return userName;
	}

    public static final Date CurrentDateTime() {
    	
    	return new Date();
    }
    
    public static float distKmFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return Math.abs(dist  / 1000);
    }
    
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
    	return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
    public static String FormatToImg64Base(String img64) {
    	return "data:image/png;base64," + img64;
    }
}

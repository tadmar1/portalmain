package com.tadmar.portal.PortalMain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PmenuR implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pmunid;
	private String pmsite;
	private String pmname;
    private String pmlink;
    private String pmsimg;
    private String pmstxt;
    private String pmatnm;
    private String pmatch;
    private String pmrgid;
    private String pmrgin;
    private Date pmrgdt;
	public Integer getPmunid() {
		return pmunid;
	}
	public String getPmsite() {
		return pmsite;
	}
	public void setPmsite(String pmsite) {
		this.pmsite = pmsite;
	}
	public void setPmunid(Integer pmunid) {
		this.pmunid = pmunid;
	}
	public String getPmname() {
		return pmname;
	}
	public void setPmname(String pmname) {
		this.pmname = pmname;
	}
	public String getPmlink() {
		return pmlink;
	}
	public void setPmlink(String pmlink) {
		this.pmlink = pmlink;
	}
	public String getPmsimg() {
		return pmsimg;
	}
	public void setPmsimg(String pmsimg) {
		this.pmsimg = pmsimg;
	}
	public String getPmstxt() {
		return pmstxt;
	}
	public void setPmstxt(String pmstxt) {
		this.pmstxt = pmstxt;
	}
	public String getPmatnm() {
		return pmatnm;
	}
	public void setPmatnm(String pmatnm) {
		this.pmatnm = pmatnm;
	}
	public String getPmatch() {
		return pmatch;
	}
	public void setPmatch(String pmatch) {
		this.pmatch = pmatch;
	}
	public String getPmrgid() {
		return pmrgid;
	}
	public void setPmrgid(String pmrgid) {
		this.pmrgid = pmrgid;
	}
	public String getPmrgin() {
		return pmrgin;
	}
	public void setPmrgin(String pmrgin) {
		this.pmrgin = pmrgin;
	}
	public Date getPmrgdt() {
		return pmrgdt;
	}
	public void setPmrgdt(Date pmrgdt) {
		this.pmrgdt = pmrgdt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pmatch == null) ? 0 : pmatch.hashCode());
		result = prime * result + ((pmatnm == null) ? 0 : pmatnm.hashCode());
		result = prime * result + ((pmlink == null) ? 0 : pmlink.hashCode());
		result = prime * result + ((pmname == null) ? 0 : pmname.hashCode());
		result = prime * result + ((pmrgdt == null) ? 0 : pmrgdt.hashCode());
		result = prime * result + ((pmrgid == null) ? 0 : pmrgid.hashCode());
		result = prime * result + ((pmrgin == null) ? 0 : pmrgin.hashCode());
		result = prime * result + ((pmsimg == null) ? 0 : pmsimg.hashCode());
		result = prime * result + ((pmstxt == null) ? 0 : pmstxt.hashCode());
		result = prime * result + ((pmunid == null) ? 0 : pmunid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PmenuR))
			return false;
		PmenuR other = (PmenuR) obj;
		if (pmatch == null) {
			if (other.pmatch != null)
				return false;
		} else if (!pmatch.equals(other.pmatch))
			return false;
		if (pmatnm == null) {
			if (other.pmatnm != null)
				return false;
		} else if (!pmatnm.equals(other.pmatnm))
			return false;
		if (pmlink == null) {
			if (other.pmlink != null)
				return false;
		} else if (!pmlink.equals(other.pmlink))
			return false;
		if (pmname == null) {
			if (other.pmname != null)
				return false;
		} else if (!pmname.equals(other.pmname))
			return false;
		if (pmrgdt == null) {
			if (other.pmrgdt != null)
				return false;
		} else if (!pmrgdt.equals(other.pmrgdt))
			return false;
		if (pmrgid == null) {
			if (other.pmrgid != null)
				return false;
		} else if (!pmrgid.equals(other.pmrgid))
			return false;
		if (pmrgin == null) {
			if (other.pmrgin != null)
				return false;
		} else if (!pmrgin.equals(other.pmrgin))
			return false;
		if (pmsimg == null) {
			if (other.pmsimg != null)
				return false;
		} else if (!pmsimg.equals(other.pmsimg))
			return false;
		if (pmstxt == null) {
			if (other.pmstxt != null)
				return false;
		} else if (!pmstxt.equals(other.pmstxt))
			return false;
		if (pmunid == null) {
			if (other.pmunid != null)
				return false;
		} else if (!pmunid.equals(other.pmunid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PmenuR [pmunid=" + pmunid + ", pmname=" + pmname + ", pmlink="
				+ pmlink + ", pmsimg=" + pmsimg + ", pmstxt=" + pmstxt
				+ ", pmatnm=" + pmatnm + ", pmatch=" + pmatch + ", pmrgid="
				+ pmrgid + ", pmrgin=" + pmrgin + ", pmrgdt=" + pmrgdt + "]";
	}
}

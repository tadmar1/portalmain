package com.tadmar.portal.PortalMain.controller;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.tadmar.portal.PortalMain.data.PmenuRepo;
import com.tadmar.portal.PortalMain.data.shared.EnvirRepo;
import com.tadmar.portal.PortalMain.model.MobileMenu;
import com.tadmar.portal.PortalMain.model.PmenuAll;
//import com.tadmar.portal.PortalMain.model.PmenuC;
import com.tadmar.portal.PortalMain.model.PmenuF;
import com.tadmar.portal.PortalMain.model.PmenuFav;
import com.tadmar.portal.PortalMain.model.PmenuL;
import com.tadmar.portal.PortalMain.model.PmenuR;
import com.tadmar.portal.PortalMain.model.shared.CusrDa;
import com.tadmar.portal.PortalMain.util.Utils;
import com.tadmar.utils.faces.FacesUtils;

@Named(value="MainCtrl")
@ViewScoped
public class MainCtrl implements Serializable{

	/**
	 * 
	 */
	public static final Integer FAV_ID = 1;
	public static final Integer LEVEL_HEADER = 1;
	public static final Integer LEVEL_LINE = 2;
	public static final Integer TYPE_ALL = 1;
	public static final Integer TYPE_USER = 2;
	public static final String SITE_NAME="Main";
	
	private static final long serialVersionUID = 1L;
	
	private Date currDate;
	private String currentSite;
	private PmenuAll pmenuAll;
	private List<PmenuAll> pmenuAllList;
	private List<PmenuFav> pmenuFavList;
	private PmenuF pmenuF;
	private PmenuL pmenuL;
	private PmenuF pmenuFSelected;
	private List<PmenuF> pmenuFList;
	private List<PmenuAll> menuSalesCentralList;
	private boolean menuSalesCentral;
	
	private String pmenuLinkFiles;
	
	//private List<PmenuC> pmenuCList;
	private List<PmenuR> pmenuRList;
	private List<PmenuR> pmenuR0List;
	private List<PmenuR> pmenuR1List;
	
	private boolean visiblePortalAdd = false;
	
	@Inject private PmenuRepo pmenuRepo;
	
	private CusrDa cusrda;
	@Inject private EnvirRepo envirRepo;
	@Inject private FacesUtils facesUtils;
	private String clientIp;
	private String clientId;
	private int taskNo;

	
	/********************************************************************************************************
	 *  Constructors
	 ********************************************************************************************************/
	@PostConstruct
	private void init() {
		currentSite = SITE_NAME;
		String userSgid = "";
		try {
		userSgid = Utils.getUserName();
		cusrda = envirRepo.findById(userSgid);
		if (cusrda == null || cusrda.getCusrid() == null) {
			facesUtils.redirectPageToLoginError();
		}
		pmenuAllList = LinkTrimStringL(pmenuRepo.findMenuAll(cusrda.getCusrid(), currentSite));
		pmenuFavList = pmenuRepo.findMenuFav(cusrda.getCusrid(), currentSite);
		pmenuFList = pmenuFavList.get(0).getPmline();
		//pmenuCList = pmenuRepo.findAllPmenuC(currentSite);
		pmenuRList = LinkTrimStringR(pmenuRepo.findAllPmenuR(currentSite));
		pmenuR0List = findRightMenuByType("0");
		pmenuR1List = findRightMenuByType("1");
		setMenuSalesCentralList(findMenuSalesCentralMenu());
		menuSalesCentral = false;
		pmenuF = new PmenuF();
		currDate = new Date();
		clientIp = org.omnifaces.util.Faces.getRemoteAddr();
		clientId = userSgid;
		taskNo = 0;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	/********************************************************************************************************
	 *  Events
	 ********************************************************************************************************/
	public void onfavToolsBtn() {
		pmenuFList = pmenuFavList.get(0).getPmline();
		pmenuF = new PmenuF();
	}
	
	public void onfavToolsBtnRefresh() {
		pmenuFavList = pmenuRepo.findMenuFav(cusrda.getCusrid(), currentSite);
	}
	
	public void onFavListSelect() {
		pmenuF = pmenuFSelected;
	}

	public void onFavListNew() {
		pmenuF = new PmenuF();
	}
	
	public void onFavListSave() {
		if (pmenuF.getPmname() == null || pmenuF.getPmname().isEmpty() || 
				pmenuF.getPmlink() == null || pmenuF.getPmlink().isEmpty()	) {
			Utils.dspMsg(FacesMessage.SEVERITY_ERROR, "Dane nie zostały wpisane ...");
			return;
		}
		SaveFav(pmenuF);
		Utils.dspMsg(FacesMessage.SEVERITY_INFO, "Pozycja dopisna do Ulubionych ...");
		pmenuFavList = pmenuRepo.findMenuFav(cusrda.getCusrid(), currentSite);
		pmenuFList = pmenuFavList.get(0).getPmline();
		pmenuF = new PmenuF();
	}
	
	public void onFavListRemove() {
		if (pmenuFSelected == null ) {
			Utils.dspMsg(FacesMessage.SEVERITY_ERROR, "Brak wybranej pozycji z listy ...");
			return;
		}
		
		RemoveFav(pmenuF);
		Utils.dspMsg(FacesMessage.SEVERITY_INFO, "Pozycja usunięta z Ulubionych ...");
		pmenuFavList = pmenuRepo.findMenuFav(cusrda.getCusrid(), currentSite);
		pmenuFList = pmenuFavList.get(0).getPmline();
		pmenuF = new PmenuF();
	}

	public void onRightMenu() {
		menuSalesCentral = true;
	}
	
	public void onLoadPortalAdd() {
		visiblePortalAdd = true;
		facesUtils.updateUiWidget("idPortalAdd");
		
	}
	
	public void onRefreshTaskData() {
		if (cusrda.getCusrid() != null) {
			List<MobileMenu> mobileMenuList = envirRepo.findMenuForDesktop("%");
			taskNo = 0;
			for (MobileMenu mobileMenu : mobileMenuList) {
				taskNo += envirRepo.findTasksNo(mobileMenu.getMmwqry().replace("{user}", cusrda.getCusrid().toUpperCase().trim()));
			}
		}
	}

	public boolean weblink(String link) {
		
		return link.toUpperCase().startsWith("HTTP") ? true : false;
	}
	
	public void onWebLinkShow() throws IOException {
		
		//return "http://www.google.com?faces-redirect=true;";
		FacesContext context = FacesContext.getCurrentInstance();
		var currLink = context.getExternalContext().getRequestParameterMap().get("pmlink");
		
		if ( currLink != null && currLink.toUpperCase().startsWith("HTTP")) {
			FacesContext.getCurrentInstance().getExternalContext().redirect(currLink.trim());
		} else {
			currLink = URLEncoder.encode(currLink, StandardCharsets.UTF_8.toString());
			var redirectUrl = "pages/linkF.jsf?linkf=" + currLink.trim();
			FacesContext.getCurrentInstance().getExternalContext().redirect(redirectUrl);
		}
	}
	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/
	private void SaveFav(PmenuF pmenuF) {
		
		if (pmenuF.getPmunid() == null) {
			pmenuF.setPmsite(SITE_NAME);
			pmenuF.setPmlevl(LEVEL_LINE);
			pmenuF.setPmpaid(FAV_ID);
			pmenuF.setPmtype(TYPE_USER);
			pmenuF.setPmuser(cusrda.getCusrid().toUpperCase().trim());
			pmenuF.setPmrgid(cusrda.getCusrid().toUpperCase().trim());
			pmenuF.setPmrgin(cusrda.getCusrnm().toUpperCase().trim());
			pmenuF.setPmrgdt(new Date());
		}
		if (! pmenuF.getPmlink().toLowerCase().startsWith("http")) {
			pmenuF.setPmlink("http://" + pmenuF.getPmlink());
		}
		pmenuF.setPmlmid(cusrda.getCusrid().toUpperCase().trim());
		pmenuF.setPmlmnm(cusrda.getCusrnm().toUpperCase().trim());
		pmenuF.setPmlmdt(new Date());
		pmenuRepo.createOrSavePmenuF(pmenuF);
	}
	
	private List<PmenuAll> findMenuSalesCentralMenu() {
	
		return pmenuAllList.stream().filter(p -> p.getPmheader().getPmunid() == 31).collect(Collectors.toList());
	}
	
	private List<PmenuR> findRightMenuByType(String type) {
		List<PmenuR> pmenuRL = new ArrayList<PmenuR>();
		for (PmenuR pmenuR : pmenuRList) {
			if (pmenuR.getPmsimg().trim().equals(type)) {
				pmenuRL.add(pmenuR);
			}
		}
		return pmenuRL;
	}

	private void RemoveFav(PmenuF pmenuF) {
		pmenuRepo.removePmenuF(pmenuF);
	}
	
	private List<PmenuR> LinkTrimStringR(List<PmenuR> pmenuRList) {
		
		for(PmenuR pmenuR : pmenuRList) {
			pmenuR.setPmlink(pmenuR.getPmlink().trim());
		}
		return pmenuRList;
	}

	private List<PmenuAll> LinkTrimStringL(List<PmenuAll> pmenuAllList ) {
		for (PmenuAll pmenuAll :pmenuAllList  ) {
			for ( PmenuL pmenuL : pmenuAll.getPmline()) {
				pmenuL.setPmlink(pmenuL.getPmlink().trim());
			}
		}
		return pmenuAllList;
	}
	/********************************************************************************************************
	 *  Get-Set
	 ********************************************************************************************************/
	public PmenuAll getPmenuAll() {
		return pmenuAll;
	}
	public void setPmenuAll(PmenuAll pmenuAll) {
		this.pmenuAll = pmenuAll;
	}
	public List<PmenuFav> getPmenuFavList() {
		return pmenuFavList;
	}

	public void setPmenuFavList(List<PmenuFav> pmenuFavList) {
		this.pmenuFavList = pmenuFavList;
	}

	public List<PmenuAll> getPmenuAllList() {
		return pmenuAllList;
	}
	public void setPmenuAllList(List<PmenuAll> pmenuAllList) {
		this.pmenuAllList = pmenuAllList;
	}

	public CusrDa getCusrda() {
		return cusrda;
	}

	public void setCusrda(CusrDa cusrda) {
		this.cusrda = cusrda;
	}

	public PmenuL getPmenuL() {
		return pmenuL;
	}

	public void setPmenuL(PmenuL pmenuL) {
		this.pmenuL = pmenuL;
	}

	public PmenuF getPmenuF() {
		return pmenuF;
	}

	public void setPmenuF(PmenuF pmenuF) {
		this.pmenuF = pmenuF;
	}
	//public List<PmenuC> getPmenuCList() {
	//	return pmenuCList;
	//}
	//public void setPmenuCList(List<PmenuC> pmenuCList) {
	//	this.pmenuCList = pmenuCList;
	//}
	public List<PmenuR> getPmenuRList() {
		return pmenuRList;
	}
	public void setPmenuRList(List<PmenuR> pmenuRList) {
		this.pmenuRList = pmenuRList;
	}
	public PmenuF getPmenuFSelected() {
		return pmenuFSelected;
	}
	public void setPmenuFSelected(PmenuF pmenuFSelected) {
		this.pmenuFSelected = pmenuFSelected;
	}
	public List<PmenuF> getPmenuFList() {
		return pmenuFList;
	}
	public void setPmenuFList(List<PmenuF> pmenuFList) {
		this.pmenuFList = pmenuFList;
	}
	public String getCurrentSite() {
		return currentSite;
	}
	public void setCurrentSite(String currentSite) {
		this.currentSite = currentSite;
	}
	public Date getCurrDate() {
		return currDate;
	}
	public void setCurrDate(Date currDate) {
		this.currDate = currDate;
	}
	public boolean isMenuSalesCentral() {
		return menuSalesCentral;
	}
	public void setMenuSalesCentral(boolean menuSalesCentral) {
		this.menuSalesCentral = menuSalesCentral;
	}

	public List<PmenuAll> getMenuSalesCentralList() {
		return menuSalesCentralList;
	}

	public void setMenuSalesCentralList(List<PmenuAll> menuSalesCentralList) {
		this.menuSalesCentralList = menuSalesCentralList;
	}

	public List<PmenuR> getPmenuR0List() {
		return pmenuR0List;
	}

	public void setPmenuR0List(List<PmenuR> pmenuR0List) {
		this.pmenuR0List = pmenuR0List;
	}

	public List<PmenuR> getPmenuR1List() {
		return pmenuR1List;
	}

	public void setPmenuR1List(List<PmenuR> pmenuR1List) {
		this.pmenuR1List = pmenuR1List;
	}

	public boolean isVisiblePortalAdd() {
		return visiblePortalAdd;
	}

	public void setVisiblePortalAdd(boolean visiblePortalAdd) {
		this.visiblePortalAdd = visiblePortalAdd;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public int getTaskNo() {
		return taskNo;
	}

	public void setTaskNo(int taskNo) {
		this.taskNo = taskNo;
	}

	public String getPmenuLinkFiles() {
		return pmenuLinkFiles;
	}

	public void setPmenuLinkFiles(String pmenuLinkFiles) {
		this.pmenuLinkFiles = pmenuLinkFiles;
	}
}

package com.tadmar.portal.PortalMain.model.shared;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CusrDa implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String cusrid;	// user SGID
	private String cusrnm;	//user name
	private String cucosy;  // cost center symbol
	private String cuconm;  // cost center name
	private String cupaid;	// user parent id
	private String cupanm;  // user parent name
	private String curoid;	// user role id
	private String curonm;	// user role name
	private String cucopi;	// user parent cost center id
	private String cusrma;	// user mail
	private String cusrmv;	// user movex
	public String getCusrid() {
		return cusrid;
	}
	public void setCusrid(String cusrid) {
		this.cusrid = cusrid;
	}
	public String getCusrnm() {
		return cusrnm;
	}
	public void setCusrnm(String cusrnm) {
		this.cusrnm = cusrnm;
	}
	public String getCucosy() {
		return cucosy;
	}
	public void setCucosy(String cucosy) {
		this.cucosy = cucosy;
	}
	public String getCuconm() {
		return cuconm;
	}
	public void setCuconm(String cuconm) {
		this.cuconm = cuconm;
	}
	public String getCupaid() {
		return cupaid;
	}
	public void setCupaid(String cupaid) {
		this.cupaid = cupaid;
	}
	public String getCupanm() {
		return cupanm;
	}
	public void setCupanm(String cupanm) {
		this.cupanm = cupanm;
	}
	public String getCuroid() {
		return curoid;
	}
	public void setCuroid(String curoid) {
		this.curoid = curoid;
	}
	public String getCuronm() {
		return curonm;
	}
	public void setCuronm(String curonm) {
		this.curonm = curonm;
	}
	public String getCucopi() {
		return cucopi;
	}
	public void setCucopi(String cucopi) {
		this.cucopi = cucopi;
	}
	public String getCusrma() {
		return cusrma;
	}
	public void setCusrma(String cusrma) {
		this.cusrma = cusrma;
	}
	public String getCusrmv() {
		return cusrmv;
	}
	public void setCusrmv(String cusrmv) {
		this.cusrmv = cusrmv;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cuconm == null) ? 0 : cuconm.hashCode());
		result = prime * result + ((cucopi == null) ? 0 : cucopi.hashCode());
		result = prime * result + ((cucosy == null) ? 0 : cucosy.hashCode());
		result = prime * result + ((cupaid == null) ? 0 : cupaid.hashCode());
		result = prime * result + ((cupanm == null) ? 0 : cupanm.hashCode());
		result = prime * result + ((curoid == null) ? 0 : curoid.hashCode());
		result = prime * result + ((curonm == null) ? 0 : curonm.hashCode());
		result = prime * result + ((cusrid == null) ? 0 : cusrid.hashCode());
		result = prime * result + ((cusrma == null) ? 0 : cusrma.hashCode());
		result = prime * result + ((cusrmv == null) ? 0 : cusrmv.hashCode());
		result = prime * result + ((cusrnm == null) ? 0 : cusrnm.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CusrDa))
			return false;
		CusrDa other = (CusrDa) obj;
		if (cuconm == null) {
			if (other.cuconm != null)
				return false;
		} else if (!cuconm.equals(other.cuconm))
			return false;
		if (cucopi == null) {
			if (other.cucopi != null)
				return false;
		} else if (!cucopi.equals(other.cucopi))
			return false;
		if (cucosy == null) {
			if (other.cucosy != null)
				return false;
		} else if (!cucosy.equals(other.cucosy))
			return false;
		if (cupaid == null) {
			if (other.cupaid != null)
				return false;
		} else if (!cupaid.equals(other.cupaid))
			return false;
		if (cupanm == null) {
			if (other.cupanm != null)
				return false;
		} else if (!cupanm.equals(other.cupanm))
			return false;
		if (curoid == null) {
			if (other.curoid != null)
				return false;
		} else if (!curoid.equals(other.curoid))
			return false;
		if (curonm == null) {
			if (other.curonm != null)
				return false;
		} else if (!curonm.equals(other.curonm))
			return false;
		if (cusrid == null) {
			if (other.cusrid != null)
				return false;
		} else if (!cusrid.equals(other.cusrid))
			return false;
		if (cusrma == null) {
			if (other.cusrma != null)
				return false;
		} else if (!cusrma.equals(other.cusrma))
			return false;
		if (cusrmv == null) {
			if (other.cusrmv != null)
				return false;
		} else if (!cusrmv.equals(other.cusrmv))
			return false;
		if (cusrnm == null) {
			if (other.cusrnm != null)
				return false;
		} else if (!cusrnm.equals(other.cusrnm))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CusrDa [cusrid=" + cusrid + ", cusrnm=" + cusrnm + ", cucosy="
				+ cucosy + ", cuconm=" + cuconm + ", cupaid=" + cupaid
				+ ", cupanm=" + cupanm + ", curoid=" + curoid + ", curonm="
				+ curonm + ", cucopi=" + cucopi + ", cusrma=" + cusrma
				+ ", cusrmv=" + cusrmv + "]";
	}
}

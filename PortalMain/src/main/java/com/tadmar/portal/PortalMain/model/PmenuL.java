package com.tadmar.portal.PortalMain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PmenuL implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer pmunid;
	private String pmsite;
	private Integer pmlevl;
	private Integer pmpaid;
	private Integer pmprio;
	private Integer pmtype;
	private String pmuser;
	private String pmname;
	private String pmlink;
	private String pmclass;
	private String pmrgid;
	private String pmrgin;
	private Date pmrgdt;
	private String pmlmid;
	private String pmlmnm;
	private Date pmlmdt;
	
	public Integer getPmunid() {
		return pmunid;
	}
	public void setPmunid(Integer pmunid) {
		this.pmunid = pmunid;
	}
	public String getPmsite() {
		return pmsite;
	}
	public void setPmsite(String pmsite) {
		this.pmsite = pmsite;
	}
	public Integer getPmlevl() {
		return pmlevl;
	}
	public void setPmlevl(Integer pmlevl) {
		this.pmlevl = pmlevl;
	}
	public Integer getPmpaid() {
		return pmpaid;
	}
	public void setPmpaid(Integer pmpaid) {
		this.pmpaid = pmpaid;
	}
	public Integer getPmprio() {
		return pmprio;
	}
	public void setPmprio(Integer pmprio) {
		this.pmprio = pmprio;
	}
	public Integer getPmtype() {
		return pmtype;
	}
	public void setPmtype(Integer pmtype) {
		this.pmtype = pmtype;
	}
	public String getPmuser() {
		return pmuser;
	}
	public void setPmuser(String pmuser) {
		this.pmuser = pmuser;
	}
	public String getPmname() {
		return pmname;
	}
	public void setPmname(String pmname) {
		this.pmname = pmname;
	}
	public String getPmlink() {
		return pmlink;
	}
	public void setPmlink(String pmlink) {
		this.pmlink = pmlink;
	}
	public String getPmclass() {
		return pmclass;
	}
	public void setPmclass(String pmclass) {
		this.pmclass = pmclass;
	}
	public String getPmrgid() {
		return pmrgid;
	}
	public void setPmrgid(String pmrgid) {
		this.pmrgid = pmrgid;
	}
	public String getPmrgin() {
		return pmrgin;
	}
	public void setPmrgin(String pmrgin) {
		this.pmrgin = pmrgin;
	}
	public Date getPmrgdt() {
		return pmrgdt;
	}
	public void setPmrgdt(Date pmrgdt) {
		this.pmrgdt = pmrgdt;
	}
	public String getPmlmid() {
		return pmlmid;
	}
	public void setPmlmid(String pmlmid) {
		this.pmlmid = pmlmid;
	}
	public String getPmlmnm() {
		return pmlmnm;
	}
	public void setPmlmnm(String pmlmnm) {
		this.pmlmnm = pmlmnm;
	}
	public Date getPmlmdt() {
		return pmlmdt;
	}
	public void setPmlmdt(Date pmlmdt) {
		this.pmlmdt = pmlmdt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pmclass == null) ? 0 : pmclass.hashCode());
		result = prime * result + ((pmlevl == null) ? 0 : pmlevl.hashCode());
		result = prime * result + ((pmlink == null) ? 0 : pmlink.hashCode());
		result = prime * result + ((pmlmdt == null) ? 0 : pmlmdt.hashCode());
		result = prime * result + ((pmlmid == null) ? 0 : pmlmid.hashCode());
		result = prime * result + ((pmlmnm == null) ? 0 : pmlmnm.hashCode());
		result = prime * result + ((pmname == null) ? 0 : pmname.hashCode());
		result = prime * result + ((pmrgdt == null) ? 0 : pmrgdt.hashCode());
		result = prime * result + ((pmrgid == null) ? 0 : pmrgid.hashCode());
		result = prime * result + ((pmrgin == null) ? 0 : pmrgin.hashCode());
		result = prime * result + ((pmtype == null) ? 0 : pmtype.hashCode());
		result = prime * result + ((pmunid == null) ? 0 : pmunid.hashCode());
		result = prime * result + ((pmuser == null) ? 0 : pmuser.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PmenuL))
			return false;
		PmenuL other = (PmenuL) obj;
		if (pmclass == null) {
			if (other.pmclass != null)
				return false;
		} else if (!pmclass.equals(other.pmclass))
			return false;
		if (pmlevl == null) {
			if (other.pmlevl != null)
				return false;
		} else if (!pmlevl.equals(other.pmlevl))
			return false;
		if (pmlink == null) {
			if (other.pmlink != null)
				return false;
		} else if (!pmlink.equals(other.pmlink))
			return false;
		if (pmlmdt == null) {
			if (other.pmlmdt != null)
				return false;
		} else if (!pmlmdt.equals(other.pmlmdt))
			return false;
		if (pmlmid == null) {
			if (other.pmlmid != null)
				return false;
		} else if (!pmlmid.equals(other.pmlmid))
			return false;
		if (pmlmnm == null) {
			if (other.pmlmnm != null)
				return false;
		} else if (!pmlmnm.equals(other.pmlmnm))
			return false;
		if (pmname == null) {
			if (other.pmname != null)
				return false;
		} else if (!pmname.equals(other.pmname))
			return false;
		if (pmrgdt == null) {
			if (other.pmrgdt != null)
				return false;
		} else if (!pmrgdt.equals(other.pmrgdt))
			return false;
		if (pmrgid == null) {
			if (other.pmrgid != null)
				return false;
		} else if (!pmrgid.equals(other.pmrgid))
			return false;
		if (pmrgin == null) {
			if (other.pmrgin != null)
				return false;
		} else if (!pmrgin.equals(other.pmrgin))
			return false;
		if (pmtype == null) {
			if (other.pmtype != null)
				return false;
		} else if (!pmtype.equals(other.pmtype))
			return false;
		if (pmunid == null) {
			if (other.pmunid != null)
				return false;
		} else if (!pmunid.equals(other.pmunid))
			return false;
		if (pmuser == null) {
			if (other.pmuser != null)
				return false;
		} else if (!pmuser.equals(other.pmuser))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PmenuL [pmunid=" + pmunid + ", pmlevl=" + pmlevl + ", pmtype="
				+ pmtype + ", pmuser=" + pmuser + ", pmname=" + pmname
				+ ", pmlink=" + pmlink + ", pmclass=" + pmclass + ", pmrgid="
				+ pmrgid + ", pmrgin=" + pmrgin + ", pmrgdt=" + pmrgdt
				+ ", pmlmid=" + pmlmid + ", pmlmnm=" + pmlmnm + ", pmlmdt="
				+ pmlmdt + "]";
	}
}

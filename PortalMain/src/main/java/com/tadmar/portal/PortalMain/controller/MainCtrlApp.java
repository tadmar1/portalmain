package com.tadmar.portal.PortalMain.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.tadmar.portal.PortalMain.data.PmenuRepo;
import com.tadmar.portal.PortalMain.model.PmenuC;

@Named(value="MainCtrlApp")
@ApplicationScoped
public class MainCtrlApp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<PmenuC> pmenuCList;
	private String currentSite;
	@Inject private PmenuRepo pmenuRepo;

	/********************************************************************************************************
	 *  Constructors
	 ********************************************************************************************************/
	@PostConstruct
	private void init() {
		currentSite = MainCtrl.SITE_NAME;
		onPmenuCSearch();
	}

	/********************************************************************************************************
	 *  Events
	 ********************************************************************************************************/
	public void onPmenuCSearch() {
		pmenuCList = pmenuRepo.findAllPmenuC(currentSite);
	}
	/********************************************************************************************************
	 *  Get-Set
	 ********************************************************************************************************/
	public List<PmenuC> getPmenuCList() {
		return pmenuCList;
	}
	public void setPmenuCList(List<PmenuC> pmenuCList) {
		this.pmenuCList = pmenuCList;
	}
}

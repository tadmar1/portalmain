package com.tadmar.portal.PortalMain.data;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tadmar.portal.PortalMain.model.PmenuAll;
import com.tadmar.portal.PortalMain.model.PmenuC;
import com.tadmar.portal.PortalMain.model.PmenuF;
import com.tadmar.portal.PortalMain.model.PmenuFav;
import com.tadmar.portal.PortalMain.model.PmenuL;
import com.tadmar.portal.PortalMain.model.PmenuR;
import com.tadmar.utils.sql.SqlExp;
import com.tadmar.utils.sql.SqlExpAnotations.SqlExpKey;

@Stateless
public class PmenuRepo {

	@Inject @SqlExpKey(key="")
	SqlExp sqlExpressionService;
	@SuppressWarnings("unused")
	@Inject private transient Logger logger;

	@PersistenceContext(unitName = "SG_PORTAL")
    private EntityManager emSp;
	
	public List<PmenuAll> findMenuAll(String userSgid, String pmsite) {
		
		List<PmenuAll> pmenuAllList = new ArrayList<PmenuAll>();
		List<PmenuL> pmenuHeaders = findMenuHeadersOrderByPriority(pmsite); 
		
		for (PmenuL pmenul : pmenuHeaders) {
			if (pmenul.getPmprio() >= 5) {
				PmenuAll pmenuAll = new PmenuAll();
				pmenuAll.setPmheader(pmenul);
				pmenuAll.setPmline(findMenuLinesOrderByPriority(userSgid,  pmenul.getPmunid()));
				pmenuAllList.add(pmenuAll);
			}
		}
		return pmenuAllList;
	}

	public List<PmenuFav> findMenuFav(String userSgid, String pmsite) {
		
		List<PmenuFav> pmenuFavList = new ArrayList<PmenuFav>();
		List<PmenuF> pmenuHeaders = findMenuHeadersFavOrderByPriority(pmsite); 
		
		for (PmenuF pmenuf : pmenuHeaders) {
			if (pmenuf.getPmprio() < 5) {
				PmenuFav pmenuFav = new PmenuFav();
				pmenuFav.setPmheader(pmenuf);
				pmenuFav.setPmline(findMenuLinesFavOrderByPriority(userSgid,  pmenuf.getPmunid()));
				pmenuFavList.add(pmenuFav);
			}
		}
		return pmenuFavList;
	}
	
	public List<PmenuL> findMenuHeadersOrderByPriority(String pmsite) {

		@SuppressWarnings("unchecked")
		List<PmenuL> pmenuLList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("PmenuRepo.findMenuHeadersOrderByPriority")
    			.replace("{0}", pmsite)
				,PmenuL.class).getResultList();
    	return pmenuLList;
	}

	public List<PmenuF> findMenuHeadersFavOrderByPriority(String pmsite) {

		@SuppressWarnings("unchecked")
		List<PmenuF> pmenuFList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("PmenuRepo.findMenuHeadersFavOrderByPriority")
    			.replace("{0}", pmsite)
				,PmenuF.class).getResultList();
    	return pmenuFList;
	}
	
	public List<PmenuF> findMenuLinesFavOrderByPriority(String userSgid, Integer pmpaid) {

		@SuppressWarnings("unchecked")
		List<PmenuF> pmenuFList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("PmenuRepo.findMenuLinesFavOrderByPriority")
    			.replace("{0}", String.valueOf(pmpaid))
    			.replace("{1}", userSgid)
				,PmenuF.class).getResultList();
    	return pmenuFList;
	}

	public List<PmenuL> findMenuLinesOrderByPriority(String userSgid, Integer pmpaid) {

		@SuppressWarnings("unchecked")
		List<PmenuL> pmenuLList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("PmenuRepo.findMenuLinesOrderByPriority")
    			.replace("{0}", String.valueOf(pmpaid))
    			.replace("{1}", userSgid)
				,PmenuL.class).getResultList();
    	return pmenuLList;
	}

	public List<PmenuC> findAllPmenuC(String pmsite) {
		@SuppressWarnings("unchecked")
		List<PmenuC> pmenuCList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("PmenuRepo.findAllPmenuC")
    			.replace("{0}", pmsite)
				,PmenuC.class).getResultList();
    	return pmenuCList;
	}

	public List<PmenuR> findAllPmenuR(String pmsite) {
		@SuppressWarnings("unchecked")
		List<PmenuR> pmenuRList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("PmenuRepo.findAllPmenuR")
    			.replace("{0}", pmsite)
				,PmenuR.class).getResultList();
    	return pmenuRList;
	}


	public void createOrSavePmenuF(PmenuF pmenuF) {
		
		if (pmenuF.getPmunid() == null) {
			emSp.persist(pmenuF);
		} else {
			emSp.merge(pmenuF);
		}
	}
	
	public void removePmenuF(PmenuF pmenuF) {
		if (emSp.contains(pmenuF)) {
	        emSp.remove(pmenuF);
	    } else {
	    	emSp.remove(emSp.merge(pmenuF));
	    }
	}
}

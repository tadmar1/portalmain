package com.tadmar.portal.PortalMain.model;

import java.io.Serializable;
import java.util.List;

public class PmenuAll implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PmenuL pmheader;
	private List<PmenuL> pmline;

	public PmenuL getPmheader() {
		return pmheader;
	}
	public void setPmheader(PmenuL pmheader) {
		this.pmheader = pmheader;
	}
	public List<PmenuL> getPmline() {
		return pmline;
	}
	public void setPmline(List<PmenuL> pmline) {
		this.pmline = pmline;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pmheader == null) ? 0 : pmheader.hashCode());
		result = prime * result + ((pmline == null) ? 0 : pmline.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PmenuAll))
			return false;
		PmenuAll other = (PmenuAll) obj;
		if (pmheader == null) {
			if (other.pmheader != null)
				return false;
		} else if (!pmheader.equals(other.pmheader))
			return false;
		if (pmline == null) {
			if (other.pmline != null)
				return false;
		} else if (!pmline.equals(other.pmline))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PmenuAll [pmheader=" + pmheader + ", pmline=" + pmline + "]";
	}
}
